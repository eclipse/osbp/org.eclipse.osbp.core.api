/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.core.api.persistence;

import java.util.Properties;

public interface IDataSourceProperties {

	public abstract String getDataSourceName(); 
	
	public abstract Properties getDataSourceProperties();
	
	public abstract void setDataSourceProperties(Properties props);
	
	public abstract void setDataSourceProperty(String key, String value);

	public abstract String getClientDriverName();

	public abstract boolean isReasonable();
}
