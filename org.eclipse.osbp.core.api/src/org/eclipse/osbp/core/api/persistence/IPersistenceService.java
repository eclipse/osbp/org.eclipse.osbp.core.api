/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.core.api.persistence;

import java.sql.SQLException;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.CommonDataSource;
import javax.sql.XAConnection;

import mondrian.olap.Result;
import mondrian.rolap.RolapConnection;

import org.eclipse.osbp.core.api.persistence.IPersistenceException.Receiver;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.component.ComponentContext;

public interface IPersistenceService {
	CommonDataSource getDataSource(String jndiName);
	java.sql.Connection getJndiConnection(String jndiName) throws SQLException;
	java.sql.Connection getPersistenceUnitConnection(String persistenceUnit) throws SQLException;
	XAConnection getXAJndiConnection(String jndiName) throws SQLException;
	XAConnection getXAPersistenceUnitConnection(String persistenceUnit) throws SQLException;
	RolapConnection getMondrianConnection(String persistenceUnit, String fullQualifiedCubePackageName) throws SQLException;
	Result sendQuery(RolapConnection con, String query);
	Result sendQuery(RolapConnection con, String query, long ms);
	void clearMondrianCache();
	void clearCubeCache(RolapConnection connection, String cubeFQN);
	void clearJpaCache();
	Filter createEMFFilter(ComponentContext context, String persistenceUnitName) throws InvalidSyntaxException;
	EntityManagerFactory getEntityManagerFactory(String persistenceUnitName);
	/**
	 * @return the local thread specific map of entity manager factories
	 */
	Map<String, EntityManagerFactory> getEntityManagerFactoryMap();
	/**
	 * @param entityManagerFactoryMap the specific map of entity manager factories to be inherited into the local thread
	 */
	void inheritEntityManagerFactoryMap(Map<String, EntityManagerFactory> entityManagerFactoryMap);
	void registerPersistenceExceptionNotification(Receiver receiver);
}
